#!/bin/bash

#set -x

RECORDING_LIFETIME=99
RECORDING="$(date +%Y-%m-%d.%H.%M).99.$RECORDING_LIFETIME.rec" 
mkdir $RECORDING
