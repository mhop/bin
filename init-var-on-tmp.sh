#!/bin/bash

set -x

VARTEMP=/tmp/var
VARPERSISTENT=/var
VARDIRS="cache log/fhem"
BINDDIRS="cache lock run"

case $1 in
    load)
        echo "creating var-directory on temp"
	[ -d $VARTEMP ] || mkdir -p $VARTEMP

	for f in $VARDIRS 
	do
            fbase=$(basename $f)
	    rsync -av $VARPERSISTENT/$fbase.bak/ $VARTEMP/$f
	done

	for f in $BINDDIRS 
	do
	    [ -d $VARTEMP/$f ] || mkdir -p $VARTEMP/$f
            mount --bind $VARTEMP/$f $VARPERSISTENT/$f
	done
	;;
    save)
        echo "saving var-directory from temp to flash"
	for f in $VARDIRS
	do
            fbase=$(basename $f)
	    [ -d $VARTEMP/$f ] && rsync -av $VARTEMP/$f/ $VARPERSISTENT/$fbase.bak 
	done
	;;
esac	

