
#scripts/feeds update
#make

cd ../ipk/ar71xx


#cp ../../base/bin/ar71xx/packages/fhem-enocean_eo_ar71xx.ipk .
#cp ../../base/bin/ar71xx/packages/fhem-trunk_trunk_ar71xx.ipk .
#cp ../../base/bin/ar71xx/packages/fhem-trunk_trunk_ar71xx.ipk .
cp ../../base/bin/ar71xx/packages/fhem*.ipk .

#perl-device-serialport_1.04-2_ar71xx.ipk
cp ../../base/bin/ar71xx/packages/perl-device-serialport_*.ipk .
#perl-weather_0.5.5-1_ar71xx.ipk
cp ../../base/bin/ar71xx/packages/perl-weather_*.ipk .
cp ../../base/bin/ar71xx/packages/eibd*.ipk .
cp ../../base/bin/ar71xx/packages/linknx*.ipk .

cp ../../base/bin/ar71xx/*.bin /home/marc/Dropbox-ipad/Public/
cp ../../base/bin/ar71xx/md5sums /home/marc/Dropbox-ipad/Public/


../../base/scripts/ipkg-make-index.sh . > Packages ; gzip -c Packages > Packages.gz
cp *.ipk /home/marc/Dropbox-ipad/Public/ipk/ar71xx/
cp Packages.gz /home/marc/Dropbox-ipad/Public/ipk/ar71xx/

cd -
