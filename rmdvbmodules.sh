#!/bin/bash

sudo rmmod budget_ci
sudo rmmod tda1004x
sudo rmmod dvb_ttpci
sudo rmmod stv0299
sudo rmmod ves1x93
sudo rmmod rc_hauppauge
sudo rmmod saa7146_vv
sudo rmmod videodev
sudo rmmod budget_core
sudo rmmod dvb_core
sudo rmmod v4l2_compat_ioctl32
sudo rmmod videobuf_dma_sg
sudo rmmod saa7146
sudo rmmod ttpci_eeprom
sudo rmmod v4l1_compat
