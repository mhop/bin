#!/bin/sh
#
# (P) & (C) 2003 by Dr. Peter Bieringer <pb at bieringer dot de>
#
# Original site of publishing:
#  ftp://ftp.bieringer.de/pub/linux/dvd/dvdauthor-addons/
#
# License: GPLv2
#
# Requires:
#  - gozer, imlib2, netpbm-progs
#
# ChangeLog
# 20030510/PB: initial creation
# 20030511/PB: import info function

. ~/.vdrconvert/vdrconvert.env

##### Functions ######

# Put text into a bitmap
put_text() {
	local file="$1"; shift
	if [ ! -z "$PATH_FONTS" -a ! -z "$DVD_USE_FONT" ]
	then
		local font="$1"
		shift
	fi
	local color_bg="$1"; shift
	local color_fg="$1"; shift
	local loc_x="$1"; shift
	local loc_y="$1"; shift
	local text="$1"; shift
	local rest="$*"

	local tmp_png="tmp-txt.png"
	local tmp_pnm="tmp-txt.pnm"
	local tmp_alphapnm="tmp-txt.alpha.pnm"
	local tmp_output="tmp-output.pnm"
	if [ ! -w "." ]; then
		echo "ERR(put_text): file not writable: $file"
		return 1
	fi

	[ ! -z "$RECODE" ] && rtext="`echo $text | $RECODE`" || rtext=$text
	if [ -z "$PATH_FONTS" -o -z "$DVD_USE_FONT" ]
	then
	#set -x
	    gozer --background $color_bg --foreground $color_fg --text "$rtext" $rest $tmp_png
	else
	    gozer --fontpath $PATH_FONTS --font $font --background $color_bg --foreground $color_fg --text "$rtext" $rest $tmp_png
	#set +x
	fi
	
	pngtopnm $tmp_png >$tmp_pnm
	pngtopnm -alpha $tmp_png >$tmp_alphapnm

	pnmcomp -xoff=$loc_x -yoff=$loc_y -alpha=$tmp_alphapnm $tmp_pnm $file >$tmp_output
	mv $tmp_output $file

	rm -f $tmp_png $tmp_pnm $tmp_alphapnm $tmp_output
}

# Put a bitmap file into another bitmap file
put_file() {
	local file=$1
	local loc_x=$2
	local loc_y=$3
	local file2=$4

	local tmp_pnm="tmp-txt.pnm"

	if [ ! -w "." ]; then
		echo "ERR(put_file): file not writable: $file"
		return 1
	fi
	if [ ! -r "$file2" ]; then
		echo "ERR(put_file): file not readable: $file2"
		return 1
	fi

	pnmcomp -xoff=$loc_x -yoff=$loc_y $file2 $file >$tmp_pnm
	mv $tmp_pnm $file

	rm -f $tmp_pnm
}
