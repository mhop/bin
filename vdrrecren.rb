#!/usr/bin/ruby

require 'socket'

def SvdrpRecv(s)
  lines=Array.new
  begin 
    str=s.readline
    lines.push(str)
    #puts("SvdrpReceived: " + str + " -  |" + str[3,1] + "|" ) if $DEBUG
  end while str[3,1]=="-"
  return(lines)
end

def SvdrpSend( s, strSvdrpCmd )
  s.write(strSvdrpCmd + "\n")
  l=SvdrpRecv(s)
  return(l)
end

def Svdrp( strSvdrpCmd )
  puts("opening socket...") if $DEBUG
  s=TCPSocket::new("localhost", 2001)
  l=SvdrpRecv(s)
  l=SvdrpSend(s, strSvdrpCmd)
  s.write("quit\r\n")
  s.close
  return(l)
end

def RenameRec( num, strDir, strName)
  if strName.size>0
    cmd=sprintf("RENR %d '%s'", num, strDir + strName)
    printf("Aufnahme %d umbennenen in '%s'...\n", num, strDir+strName)
    #Svdrp(cmd)
  end
end

if ARGV.size!=2
  printf("invalid arguments: %d\n",ARGV.size )
  exit
end

CMD=ARGV[0]
REC=ARGV[1]

if not File.directory?(REC)
  puts("not a directory")
  exit
end
RECDATEDIR=File.basename(REC)
RECPATH=File.dirname(REC)
RECNAME=File.basename(RECPATH)
BASEDIR=File.dirname(RECPATH)
EPISODENAME=File.basename(BASEDIR)

sp=RECDATEDIR.split(".")
RECDATE=sp[0]
RECHOUR=sp[1].to_i
RECMIN=sp[2].to_i
if sp[5]!="rec"
  puts("not .rec-dir")
  exit
end

sp=RECDATE.split("-")
RECDAY=sp[2].to_i
RECMON=sp[1].to_i
RECYEAR=sp[0].to_i

strTime=sprintf("%02d:%02d", RECHOUR, RECMIN);
strDate=sprintf("%02d.%02d.%02d", RECDAY, RECMON, RECYEAR % 100);

l=Svdrp("LSTR")

recs=Array.new
#printf("Date:%s ; Time:%s\n", strDate,strTime) if $DEBUG
l.each do |x|
  sp=x.split(" ")
  #printf("d:%s;t:%s\n", sp[1], sp[2])
  if sp[1]==strDate && sp[2][0,5]==strTime
    recs.push(x)
  end
end

if recs.size>1
  puts("Aufnahmezeit nicht eindeutig")
  exit
end

# Get Rec-Num
rec=recs[0][4..(recs[0].size-1)]
rec_s=rec.split(' ')
num=rec_s[0].to_i

# Get Title of Rec
title=""
rec_s[3..rec_s.size].each { |x| title=title+x+' ' }
#printf("t:%s\n",title)

# Get Dir of Rec
dir_s=title.split('~')
dir=""
dir_s[0..dir_s.size-2].each { |x| dir=dir+x+'~' }
dir_parent=dir_s[dir_s.size-2] if dir_s.size>=2 
#puts("D:"+dir)

# read info.vdr-file
infofile=REC + "/info.vdr"
if File.exists?( infofile )
  puts("reading " + infofile) if $DEBUG
  f = File.new( infofile )
  lines=f.readlines
  f.close
  info_title=""
  info_short=""
  info_desc=""
  lines.each do |l|
    if l[0,2]=="T "
      info_title=l[2..l.size].chop
    elsif l[0,2]=="S "
      info_short=l[2..l.size].chop
    elsif l[0,2]=="D "
      ld=l.size
      ld=80 if l.size > 80
      info_desc=l[2..ld].chop
    end
  end
end

puts(info_title) if $DEBUG
puts(info_short) if $DEBUG
puts(info_desc) if $DEBUG

case CMD 
when "ShortToName" :
    RenameRec(num, dir, info_short)
when "DirToName" :
    if dir_parent.size>0
      RenameRec(num, "", dir.chop) 
    end
when "DescToName" :
    RenameRec(nu, dir, info_desc)
end
