java -jar ~/bin/bfg-1.12.14.jar --delete-folders dl --delete-files dl --no-blob-protection owrttest
cd owrttest
git reflog expire --expire=now --all && git gc --prune=now --aggressive
du -sh
cd ..
java -jar ~/bin/bfg-1.12.14.jar --delete-folders scripts  --no-blob-protection owrttest
java -jar ~/bin/bfg-1.12.14.jar --delete-folders target  --no-blob-protection owrttest
java -jar ~/bin/bfg-1.12.14.jar --delete-folders toolchain  --no-blob-protection owrttest
java -jar ~/bin/bfg-1.12.14.jar --delete-folders tools  --no-blob-protection owrttest
java -jar ~/bin/bfg-1.12.14.jar --delete-folders docs  --no-blob-protection owrttest
java -jar ~/bin/bfg-1.12.14.jar --delete-folders include  --no-blob-protection owrttest
java -jar ~/bin/bfg-1.12.14.jar --delete-folders timing_log_received_sent_correlator  --no-blob-protection owrttest

