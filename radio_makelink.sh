#!/bin/bash

#set -x

radiofileindir=$1
linkdest=$(dirname "$radiofileindir")
artist=$(echo "$radiofileindir" | cut -d "/" -f 5)
album=$(echo "$radiofileindir" | cut -d "/" -f 6)

[ -d "$artist" ] || mkdir "$artist"
echo "adding: $artist/$album"
[ -x "$artist/$album" ] || ln -s "$linkdest" "$artist/$album"
 
