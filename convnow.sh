#!/bin/bash

log=/tmp/convnow.log
#echo 0:$0 >> $log
#echo 1:$1 >> $log
#echo 2:$2 >> $log

viddir=\\/var\\/lib\\/video.00\\/
recdir=$2

recpath=$(echo $recdir | sed "s/$viddir//g")
#echo recpath: $recpath >> $log 
#echo "2" >> $log

function cmd2nas {
    local cmd=$1
    local nasaddr=nas@nas
    local naskey=nas

    echo ssh -i ~/.ssh/$naskey $nasaddr "$cmd" >> $log
    ssh -i ~/.ssh/$naskey $nasaddr "$cmd"
}

#echo recpath2: $recpath >> $log 

for par in "$@"; do
    #echo "xxx:$par" >> $log
    case $par in
	vdr2mp3) 
	    #echo "vdr2mp3" >> $log
            cmd2nas "nohup ruby ~/bin/vdr2mp3.rb '$recpath' >/tmp/test.log 2>&1 & "

	    ;;
	*)
	    ;;
    esac
done

#1:vdr2mp3dd
#2:/var/lib/video.00/%Dok_5_-_Das_Feature/2012-08-05.11.00.124-0.rec
exit


ssh -i /home/marc/.ssh/nas nas@nas "nohup ruby ~/bin/vdr2mp3.rb '%Dok_5_-_Das_Feature/2012-08-05.11.00.124-0.rec' > /tmp/test.log 2>&1 &"

set -x

. /etc/vdr/vdrconvert.env

if [ ! -z "$SERVER" ]
then
    ssh $SERVER $0 $1 "'$2'"
else
    printf "%s\n" "$2" >> $VCOQUEUEDIR/$1 
fi

