#!/bin/bash
#set -x

# sudo mount -o shortname=mixed /dev/sdc1 /media/hdext/

if [ "$1" != "size" ]; then
    radiomount=$1
fi
podcastsdir=/mp3/podcast
radiolinkdir=/mp3/radio
mp3dir=/mp3/music

[ -z $radiomount ] && radiomount=/media/hdext 

[ -d $radiolinkdir ] || mkdir -p $radiolinkdir

cd $radiolinkdir
rm -r *

find $mp3dir -name radio -exec ~/bin/radio_makelink.sh {} \;

du -shL
if [ "$1" = "size" ]; then
 exit
fi
 
[ -x "$radiolinkdir/podcasts" ] || ln -s $podcastsdir "$radiolinkdir/podcasts"

rsync -rvLD --modify-window=1 --progress --delete --include='*/' --include='*.mp3' --include='*.m3u' --exclude='*' $radiolinkdir/ $radiomount/
