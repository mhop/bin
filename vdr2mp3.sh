outfile=$1.mp3


[ -z $vdrts ] && vdrts=00001.ts
[ -z $outfile ] && outfile="out.mp3"

title=""
author=""
year=$(date "+%Y")
album="vdr"


avconv -i $vdrts -acodec libmp3lame -f mp3 -metadata title=$title -metadata author=$author -metadata year=$year -metadata album=$album -ss 00:\
00:00 -ab 128k -ar 44100 -threads auto $outfile




