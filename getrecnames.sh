
hd=/media/test
videodir=/video/

recdirs=$(find . -name "*.rec")

echo $recdirs
for i in $recdirs
do
   echo rec:$i
   recdir=$(basename "$i")
   dirv=$(find $videodir -name "$recdir")
   d=$(echo $dirv | awk -F/ '{print $(NF-1)}')
   echo "$dirv = $d"
   mkdir -p $hd/$d
   mv $i $hd/$d

done
