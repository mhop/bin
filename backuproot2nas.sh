#!/bin/sh
REPOSITORY=marc@nas:/dat/bak/borg

if [ "$1" != "--prune" ]; then
borg create -v --stats --progress --compression zlib --one-file-system       \
     $REPOSITORY::'{hostname}-{now:%Y-%m-%d}'    \
     / \
     --exclude '*/tmp/*' \
     --exclude '*/tmpfile/*' \
     --exclude '/var/tmp/*' \
     --exclude '/var/crash/*' \
     --exclude '*/.cache/*' \
     --exclude '*/cache/*' \
     --exclude '*/.ccache/*' \
     --exclude '*/mlocate.db*' \
     --exclude '*/Downloads/*' \
     --exclude '/run'
fi

     

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machine's archives also.
borg prune -v --stats $REPOSITORY --prefix '{hostname}-' \
         --keep-daily=7 --keep-weekly=4 --keep-monthly=20
