

cd /home/pi/opt/vdr

sleep 60

while true; do
    running=`./svdrpsend help 2>&1 | grep version | wc -l`
    if [ $running -ne 0 ]; then
	#echo running
	:
    else
	echo "VDR stalled (1)"
	sleep 10
    	running=`./svdrpsend help 2>&1 | grep version | wc -l`
    	if [ $running -ne 0 ]; then
		echo running
    	else
		echo "VDR stalled (2) ...restarting"
		id=$(ps aux | grep "/home/pi/opt/vdr/[v]dr " | awk '{print $2; }')
		sudo kill -9 $id
		sleep 60
	fi
    fi
    sleep 10
done
