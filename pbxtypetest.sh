#!/bin/sh

url=root@192.168.178.151
broker=192.168.178.25

#topic_power="cmnd/powplug2/power"
topic_power="cmnd/plug-b/power"
log=$HOME/tmp/pbxtypetest.log


while [ true ]; do
      mosquitto_pub -h $broker -m "on" -t "$topic_power"
      sleep 50
      echo "$(date) ----------------------------------" | tee -a $log
      ssh $url 'dmesg | grep hwinfo' | tee -a $log
      ssh $url 'cat /sys/devices/platform/ff200000.hwinfo/slotmask' | tee -a $log
      ssh $url 'cat /etc/release | grep PRODUCT' | tee -a $log
      ssh $url 'cat /var/log/agfpbxtype.log' | tee -a $log
      mosquitto_pub -h $broker -m "off" -t "$topic_power"
      sleep 5
done
      

