#!/bin/sh
REPOSITORY=pi@pi4:/media/hdext/borg
#REPOSITORY=marc@nas:/dat/bak/borg_mac
#REPOSITORY=pi@kodi:/media/hdext/borg
#REPOSITORY=marc@vdr.fritz.box:/media/hdext/borg
#REPOSITORY=/Volumes/nifty/bak/borg


borg create -v --stats --progress --compression zlib --one-file-system       \
     $REPOSITORY::'{hostname}-{now:%Y-%m-%d}'    \
     /System/Volumes/Data \
     --exclude '*/tmp/*' \
     --exclude '*/tmpfile/*' \
     --exclude '*/Temp/*' \
     --exclude '*/var/tmp/*' \
     --exclude '*/var/crash/*' \
     --exclude '*/.cache/*' \
     --exclude '*/cache/*' \
     --exclude '*/Caches/*' \
     --exclude '*/.ccache/*' \
     --exclude '*/Caches/*' \
     --exclude '*/mlocate.db*' \
     --exclude '*/run' \
     --exclude '*/var/cache/apt/archives/*.deb' \
     --exclude '*/var/cache/man' \
     --exclude '*/private/var' \
     --exclude '/tmp' \
     --exclude '*/var/tmp' \
     --exclude '*/Users/marc/tmp' \
     --exclude '*/Downloads/*' \
     --exclude '*/Caches/*' \
     --exclude '*/.Trash/*' \
     --exclude '*/.Spotlight-*' \
     --exclude '*/iPhone Software Updates/*' \
     --exclude '*/ot_shared_cache' \
     --exclude '*/dyld_shared_cache*' \
     --exclude '*/Downloads/*' \
     --exclude '/System/Volumes/Data/Applications/Xcode.app/*' \

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machine's archives also.
#borg prune -v --stats $REPOSITORY --prefix '{hostname}-' \
#         --keep-daily=7 --keep-weekly=52 --keep-monthly=48
borg prune -v --stats $REPOSITORY --prefix 'Marcs-' \
         --keep-daily=7 --keep-weekly=52 --keep-monthly=48
