
#set -x

LocalDir=/dat/gps/garmin

allinone=0
radfahrer=0
kleineisel=1

# -------------------------------------------
#          AllInOne
# -------------------------------------------

if [ $allinone -eq 1 ]; then

    echo " ######## AllInOne ###############"


    #Garminfile=gmapsupp_germany.img
    Garminfile=basemap_germany.img
    Downloadfile=$Garminfile.zip

    #url_d=http://www.juergen-frank.de/osm/gmapsupps/germany/$Downloadfile
    #url_d=http://dev.openstreetmap.de/aio/germany/$Downloadfile
    #url_d=http://dev.openstreetmap.de/aio/regions/germany/gmapsupp/$Downloadfile
    url_d=ftp://ftp5.gwdg.de/pub/misc/openstreetmap/download.openstreetmap.de/aio/regions/germany/basemap/basemap_germany.img.zip
    #url_eur=http://www.juergen-frank.de/osm/gmapsupps/europe/$Downloadfile
    #url_eur=http://osm.dxp-ev.de/garmin/$Downloadfile
    url_eur=http://dev.openstreetmap.de/aio/europe/$Downloadfile

    DateFileName_d=basemap_allinone_d_
    DateFileName_eur=garmin_allinone_eur_
    TmpFile=tmp

    [ -e $LocalDir/d ] || mkdir $LocalDir/d
    cd $LocalDir/d
    wget -nv -N --retr-symlinks $url_d
    datestr=$(stat -c %y $Downloadfile | cut -f 1 -d ' ')
    DateFile=../$DateFileName_d$datestr.zip 
    if [ ! -f $DateFile ]; then
	nice -n 19 cp $Downloadfile $DateFile
    fi

    if [ $allinoneeur -eq  ]; then 
    [ -e $LocalDir/eur ] || mkdir $LocalDir/eur
    cd $LocalDir/eur
    wget -nv -N $url_eur
    datestr=$(stat -c %y $Downloadfile | cut -f 1 -d ' ')
    TarFile=../$TarFileName_eur$datestr.tar.bz2 
    if [ ! -f $TarFile ]; then 
	nice -n 19 cp $Downloadfile $TmpFile.bz2
	nice -n 19 bunzip2 $TmpFile.bz2
	mv $TmpFile $Garminfile
	nice -n 19 tar -cjf $TarFile $Garminfile
	rm $Garminfile 
        #tar -cf $TarFile gmapsupp.img.bz2 
    fi
fi

fi

# ------------ Radfahrer ----------------------

if [ $radfahrer -eq 1 ]; then 

    echo " ######## Radfahrer ##########"

    cd $LocalDir/radfahrer

    #wget -nv http://osm.arndnet.de/ -O index.html
    wget -N http://radkarte.formann.de/gmapsupp.img.zip
    #uncompress < Radkarte > Radkarte.html 
    #cp index.html  Radkarte.html 
    #url_rad=$(cat Radkarte.html | grep img\.zip | cut -f 2 -d '"')
    #wget -nv -N $url_rad
    datestr=$(stat -c %y gmapsupp.img.zip | cut -f 1 -d ' ')
    [ -f radfahrer_$datestr.zip ] || cp gmapsupp.img.zip radfahrer_$datestr.zip
    #rm Radkarte.html

fi

# --------------- Kleineisel -------------

if [ $kleineisel -eq 1 ]; then 

    echo " ######## Kleineisel ##########"

    cd $LocalDir/kleineisel
#url_klein=http://www.kleineisel.de/ralf/gps/garmin/
    url_klein=http://www.kleineisel.de/blogs/index.php/osmmap/
    wget -nv -N $url_klein
    Downloadfile=$(cat index.html | grep osm_srtm_germany_[0-9]*\.zip[^.] | cut -f 2 -d '"' |  grep  "osm_srtm_germany_[0-9]*" | head -n 1)
#wget -N $url_klein/$Downloadfile
    wget -nv -N $Downloadfile

fi
