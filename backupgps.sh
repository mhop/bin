#!/bin/sh
REPOSITORY=pi@vdrpi:/media/hdext/borg
PREFIX=nasgps
BAKDIRS="/dat/docu/gps"

borg create -v --stats --progress --compression zlib --one-file-system       \
     $REPOSITORY::$PREFIX-'{now:%Y-%m-%d}'    \
     $BAKDIRS \
     --exclude '*/tmp/*' \
     --exclude '*.iso' \
     --exclude '*.exe' \
     --exclude '*.tar*' \
     --exclude '*.tgz' \
     --exclude '*.gz' \
     --exclude '*.zip'


     

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machine's archives also.
borg prune -v --stats $REPOSITORY --prefix $PREFIX- \
         --keep-daily=7 --keep-weekly=4 --keep-monthly=20
