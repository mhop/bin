
daysback=40

srcdir=/dat/books/zeit
dstdir=/dat/docu/sync/zeit
tmpdir=/tmp

[ -d $dstdir ] || exit
[ -d $srcdir ] || exit
[ -d $tmpdir ] || exit

cd $dstdir
find . -type f | sort> $tmpdir/alt.lst

cd $srcdir
find . -mtime -$daysback -name "*.epub" | sort > $tmpdir/new.lst


rsync --files-from=$tmpdir/new.lst --progress . $dstdir/

cd $dstdir


cat $tmpdir/alt.lst $tmpdir/new.lst | sort -u  > $tmpdir/all.lst
cat $tmpdir/all.lst
cat $tmpdir/new.lst
diff $tmpdir/new.lst $tmpdir/all.lst | grep "^>" | sed  "s/^> //" | xargs rm 
# $rmfiles
