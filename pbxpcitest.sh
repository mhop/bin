#!/bin/bash

# ip=192.168.178.116
ip=172.16.99.208
url=root@$ip
#url=root@es-system
#broker=192.168.178.25
broker=172.16.170.165
sleeptime=75 
#topic_power="cmnd/powplug2/power"
#topic_power="cmnd/plug-b/power"
topic_power="shellies/rw2/whg3/buero/steckdose1/relay/0/command"
log=$HOME/tmp/pbxpcitest_$(date +%y%m%d%H%M%S).log

function stopp
{
    echo -e "\n stopped !"
    echo "{} ]"  >> $log
    exit
}

echo "Start (Stopp mit 'x')"
echo "[" >> $log
n=1
while [ true ]; do
    mosquitto_pub -h $broker -m "on" -t "$topic_power"
    echo -n "$n:on"
    #sleep 5
    read -t 5 -n 1 key
    if [ "$key" = "x" ]; then
       stopp
    fi
    i=0
    test="ok"
    echo -n " ping"
    while [ true ]; do
	read -t 0.1 -n 1 key
	if [ "$key" = "x" ]; then
	    stopp
	fi
	echo -n "."
	ping -w 1 -q $ip > /dev/null
	r=$?
	if [ $r -eq 0 ]; then
	    break
	fi
	i=$((i+1))
	if [ $i -gt 200 ]; then
	    echo "Fehler $i !"
	    test="fail"
	    break;
	fi
    done
    echo "{ \"time\" : \"$(date +%T)\", \"n\" : $n, \"test\" : \"$test\", \"result\" : $r, \"count\" : $i }," >> $log
    echo " $i off"
    mosquitto_pub -h $broker -m "off" -t "$topic_power"
    read -t 5 -n 1 key
    if [ "$key" = "x" ]; then
       stopp
    fi
    n=$((n+1))
done
stopp


exit
	
    while [ no_connect != 0 ]
	      
    sleep $sleeptime
    echo "$(date) ----------------------------------" | tee -a $log
    ssh $url -o ConnectTimeout=3 'cat /etc/release | grep PRODUCT'
    if [ $? -eq 0 ]; then 
	    echo ok >> $log
    else
	    echo fail1 >> $log
        sleep 10
        ssh $url -o ConnectTimeout=3 'cat /etc/release | grep PRODUCT'
        if [ $? -eq 0 ]; then 
	        echo ok2 >> $log
        else
	        echo fail2 >> $log
            sleep 10
            ssh $url -o ConnectTimeout=3 'cat /etc/release | grep PRODUCT'
            if [ $? -eq 0 ]; then 
	            echo ok3 >> $log
            else
	            echo fail3 >> $log
                sleep 10
                ssh $url -o ConnectTimeout=3 'cat /etc/release | grep PRODUCT'
                if [ $? -eq 0 ]; then 
	                echo ok4 >> $log
                else
                    echo fail4
                fi
            fi
        fi
    fi
    mosquitto_pub -h $broker -m "off" -t "$topic_power"
    sleep 5
done


