#!/bin/bash

set -e

export CONCURRENCY_LEVEL=3

fakeroot make-kpkg clean
fakeroot make-kpkg --initrd --append-to-version=$1 binary 
