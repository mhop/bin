
daysback=22

srcdir=/dat/books/taz
dstdir=/dat/docu/sync/taz
tmpdir=/tmp

[ -d $dstdir ] || exit
[ -d $srcdir ] || exit
[ -d $tmpdir ] || exit

cd $dstdir
find . -type f | sort> $tmpdir/alt.lst

cd $srcdir
find . -mtime -$daysback -name "*.epub" | sort > $tmpdir/new.lst

#exit

diff $tmpdir/new.lst $tmpdir/all.lst | grep "^<" | sed  "s/^< //" > $tmpdir/added.lst
rsync --files-from=$tmpdir/added.lst --progress . $dstdir/

cd $dstdir


cat $tmpdir/alt.lst $tmpdir/new.lst | sort -u  > $tmpdir/all.lst
cat $tmpdir/all.lst
cat $tmpdir/new.lst
diff $tmpdir/new.lst $tmpdir/all.lst | grep "^>" | sed  "s/^> //" | xargs rm 
# $rmfiles
