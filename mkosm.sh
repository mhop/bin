#!/bin/bash

set -x

mapfile=$1
splitter=../splitter.jar
mkgmap=../mkgmap.jar
mapid=65241345
country_name=greece

sudo swapon /dat/tmp/swap.img


#wget http://download.geofabrik.de/osm/europe/germany/$mapfile.bz2
#bunzip $mapfile.bz2

mkdir tiles
rm -r tiles/*
cd tiles/

java -Xmx5000M -jar $splitter --mapid=$mapid --max-nodes=500000 ../$mapfile

cd ..
mkdir gbasemap
rm -r gbasemap/*
cd gbasemap
java -Xmx5000M -jar $mkgmap --style-file=../styles/masterstyle --description='Openstreetmap' --country-name=$country_name --country-abbr=GR --family-id=3 --product-id=45 --series-name='master-edition' --family-name=OSM --area-name=GR --latin1 --lower-case --mapname=$mapid --draw-priority=10 --add-pois-to-areas --road-name-pois --net --route --gmapsupp ../tiles/*.osm.gz /home/marc/osm/styles/master.TYP

sudo swapoff /dat/tmp/swap.img
