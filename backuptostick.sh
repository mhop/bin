#!/bin/sh

#REPOSITORY=/media/mhoppe/KEY/bak/borg
#REPOSITORY=/media/stick/bak/borg
REPOSITORY=/media/hdext/bak/borg
name=es

prefix="buero-$name-{hostname}-"
echo $prefix
echo $REPOSITORY::$prefix{now:%Y-%m-%d}



if [ "$1" != "--prune" ]; then
	borg create -v --stats --progress --compression auto,lzma --noatime \
		 $REPOSITORY::$prefix{now:%Y-%m-%d-%H:%M:%S}    \
		 ssh://buero:/home/mhoppe/proj/$name/ \
	--exclude  '*/dl/'								 \
	--exclude  '*/update/'							 \
	--exclude  '*/build*/'							 \
	--exclude  '*/dest/'								 \
	--exclude  '*/dsp/ti'								 \
	--exclude  '*/lm_tkhs/source/user/opensource'		 \
	--exclude  '*/sdcard/'							 \
	--exclude  '*/staging/'							 \
	--exclude  '*/rootfs/'							 \
	--exclude  '*/output_files/'						 \
	--exclude  '*/synthesis/'							 \
	--exclude  '*/sourcemirror/'						 \
	--exclude  '*/downloads/'						 \
	--exclude  '*/sstate-cache/'						 \
	--exclude  '*/yocto-dl-sources.git/'					 \
	--exclude  '*/tmp*'                               \
   --exclude  '*/alt/'                               \
	--exclude  '*/*sstate/'								 \
	--exclude  '*/sstate-cache/'								 \

fi

#

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machine's archives also.
borg prune -v --stats $REPOSITORY --prefix $prefix \
	 --keep-hourly=10 --keep-daily=7 --keep-weekly=4 --keep-monthly=20

