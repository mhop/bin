#!/bin/sh

set -x

EXT_DRIVE=/mnt/hdext

REC_DIR=$1
VDR_DIR=`echo $REC_DIR | awk '{a=$0; gsub(/\/[^\/]*\.rec/,"",$a);print $a}'`
REC_NAME=`echo $REC_DIR | awk -F/ '{print $(NF-1)}'`

mount $EXT_DRIVE

if [ -f $EXT_DRIVE/vdrhd.num -a -f $REC_DIR/dvd.vdr ]; then
    cmp $REC_DIR/dvd.vdr $EXT_DRIVE/vdrhd.num
    if [ $? -eq 0 ]; then
        svdrpsend.pl MESG "Ausgelagert l�schen gestarted"
        EXT_DIR="$(find ${EXT_DRIVE} -name $REC_NAME)"
        if [ -d "$EXT_DIR" ]; then
            EXT_DIR_PAR=$(echo $EXT_DIR | awk '{a=$0; gsub(/\/[^\/]*\.rec/,"",$a);print $a}')
            rm -r $EXT_DIR_PAR
            svdrpsend.pl MESG "l�schen abgeschlossen"
            umount $EXT_DRIVE
            exit
        else
            svdrpsend.pl MESG "externe Aufnahme nicht gefunden"
        fi
    else
        svdrpsend.pl MESG "Laufwerksnummer pa�t nicht"
    fi	
else
    svdrpsend.pl MESG "Kein DVD-Archive Laufwerk"
fi

umount $EXT_DRIVE



