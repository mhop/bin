#!/bin/bash

#set -x

# Dieses Script versucht bei jedem Aufruf, die aktuelle taz herunterzuladen. 
# Bei Problemen wird per E-Mail eine Warnung an mich geschickt.

# So sehen die Dateien aus:
# http://www.taz.de/cgi-bin/digiabo/2007_04_17_HTM.zip

TAZDIR=/dat/books/taz
TAZUSER=103450
TAZPASSWD=oxculo
TAZTYPE=.pdf
TAZTYPE_EPUB=.epub
TAZURL=http://www.taz.de/taz/abo/get.php?f=
EMAIL=MarcHoppe@gmx.de

function download {
    echo "Download $2"
    [ -f $1 ] || wget -c --http-user $TAZUSER --http-password $TAZPASSWD -O $1 $2;
    [ $(stat -c%s $1) -lt 1000 ] && rm $1
}

cd $TAZDIR

TIMESTAMP_FILE=last_download.txt;
source $TIMESTAMP_FILE;
ZEIT=$(date +%s);
echo "LASTDOWNLOAD=$ZEIT" > $TIMESTAMP_FILE;
let TIME_DIFFERENCE=$ZEIT-$LASTDOWNLOAD;
let "TODAYS_SECOND  = ($(date +%k) + $(date +%M) * 60) * 60 + $(date +%S)";
let "SECONDS_SINCE_LAST_MIDNIGHT = 86400 + $TODAYS_SECOND";

# echo "Sekunden seit letztem Aufruf: $TIME_DIFFERENCE";

# Was the last call before yesterday?
#if [ "$TIME_DIFFERENCE" -gt "$SECONDS_SINCE_LAST_MIDNIGHT" ]
#then
#  let "HOURS_OFF = $TIME_DIFFERENCE / 3600";
#  echo "Rechner war heute und gestern nicht an, war $HOURS_OFF Stunden aus! taz-Ausgabe könnte fehlen!" | mail -s "taz-Herunterladeproblem" $EMAIL;
#fi;


# There is no taz on Sundays (German Sonntag)
#if [ "So" != "$(date +%a)" ]
#then
  TAG[1]=$(date -d tomorrow +"%Y_%m_%d");
  TAG[2]=$(date -d yesterday +"%Y_%m_%d");
  TAG[3]=$(date +"%Y_%m_%d");
  TAG[4]=$(date -d '-2 days' +"%Y_%m_%d");
  TAG[5]=$(date -d '-3 days' +"%Y_%m_%d");
  TAG[6]=$(date -d '-4 days' +"%Y_%m_%d");
  TAG[7]=$(date -d '-5 days' +"%Y_%m_%d");
  TAG[8]=$(date -d '-6 days' +"%Y_%m_%d");
  TAG[9]=$(date -d '-7 days' +"%Y_%m_%d");
  TAG[10]=$(date -d '-8 days' +"%Y_%m_%d");
  TAG[11]=$(date -d '-9 days' +"%Y_%m_%d");
  TAG[12]=$(date -d '-10 days' +"%Y_%m_%d");
  TAG[13]=$(date -d '-11 days' +"%Y_%m_%d");
  TAG[14]=$(date -d '-12 days' +"%Y_%m_%d");
  TAG[15]=$(date -d '-13 days' +"%Y_%m_%d");
  TAG[16]=$(date -d '-14 days' +"%Y_%m_%d");
#    echo "Würde ""$TAG""$TAZTYPE herunterladen...";
for i in {1..16}
do
  download "${TAG[$i]}""$TAZTYPE" "$TAZURL""${TAG[$i]}""$TAZTYPE"
done

sleep 5
for i in {1..16}
do
    download "${TAG[$i]}""$TAZTYPE_EPUB" "$TAZURL""taz_""${TAG[$i]}""$TAZTYPE_EPUB"
done

# Sync to Dropbox
bash $HOME/bin/tazsync.sh
