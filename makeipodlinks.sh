#!/bin/bash
set -x


ipodlinkdir=/mp3/ipod
mp3dir=/mp3/music


cd $ipodlinkdir

find $mp3dir -name ipod -exec ~/bin/makeipodlink.sh {} \;

