#!/bin/sh

LOGFILE=/tmp/podcatcher.log
podcastdir=/dat/audio/podcast

set -x
sleep 1
#swapon /dat/tmp/swap.img
#svdrpsend MESG "Podcasts aktualisieren-gestarted"
cd $podcastdir
echo "start ----------------------------" >> $LOGFILE
date >> $LOGFILE
./catch2.sh >> $LOGFILE 2>&1
#svdrpsend MESG "Podcasts aktualisiert"
#swapoff /dat/tmp/swap.img
