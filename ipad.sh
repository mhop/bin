#!/bin/bash


ipaddir=/dat/ipad

rm -rf /dat/ipad/*

# taz
#tazdir=/dat/books/taz
#files=$(ls $tazdir/*.epub)
#dstdir=$ipaddir/books/taz/
#mkdir -p $dstdir
#for f in $files; do
#    if [ ! -e "$dstdir/$f" ]; then
#	ln -s "$f" $dstdir
#    fi
#done

# Bücher dropbox epub
filedir="/home/marc/Dropbox/Elektronische Bücher (1)"
dstdir=$ipaddir/books
mkdir $dstdir
find "$filedir/"  -iname "*.epub" -print0 | while read -d $'\0' f
do
    if [ ! -e "$dstdir/$f" ]; then
		ln -s "$f" $dstdir
    fi
done

# Bücher dropbox pdf
filedir="/home/marc/Dropbox/Elektronische Bücher (1)"
dstdir=$ipaddir/books
find "$filedir/"  -iname "*.pdf" -print0 | while read -d $'\0' f
do
    b=$filedir/$(basename "$dstdir/$f" .pdf)
    #echo $b
    if [ ! -e "$b.epub" ]; then 
		if [ ! -e "$dstdir/$f" ]; then
			ln -s "$f" $dstdir
		fi
    fi
done

# Podcasts
#filedir="/dat/mp3/podcast/cache"
#dstdir=$ipaddir/
#ln -s $filedir $dstdir/podcast

# Musik
linkdir=$ipaddir/music
mp3dir=/mp3/music

[ -d $linkdir ] || mkdir -p $linkdir

cd $linkdir
rm -r *

find $mp3dir -name ipad -print0 | while read -d $'\0' f
do
    fileindir=$f
    linkdest=$(dirname "$fileindir")
    artist=$(echo "$fileindir" | cut -d "/" -f 5)
    album=$(echo "$fileindir" | cut -d "/" -f 6)

    [ -d "$artist" ] || mkdir "$artist"
    echo "adding: $artist/$album"
    [ -x "$artist/$album" ] || ln -s "$linkdest" "$artist/$album"
done

# Hoerspiele
linkdir=$ipaddir/hoerspiel
mp3dir=/mp3/Hörspiel

[ -d $linkdir ] || mkdir -p $linkdir

cd $linkdir
rm -r *

find $mp3dir -name ipad -print0 | while read -d $'\0' f
do
    fileindir=$f
    linkdest=$(dirname "$fileindir")
    #artist=$(echo "$fileindir" | cut -d "/" -f 5)
    #album=$(echo "$fileindir" | cut -d "/" -f 6)

    #[ -d "$artist" ] || mkdir "$artist"
    echo "adding: $f"
    ln -s "$linkdest"
done

# Bilder
dstdir=$ipaddir/bilder
srcdir=/dat/bilder

[ -d $dstdir ] || mkdir -p $dstdir

cd $dstdir || exit
[ "$dstdir" != "" ] && rm -r *

find $srcdir -name ipad -print0 | while read -d $'\0' f
do
    fileindir=$f
    linkdest=$(dirname "$fileindir")
    echo "adding: $f"
    ln -s "$linkdest"

    #[ -x "$artist/$album" ] || ln -s "$linkdest" "$artist/$album"
done
