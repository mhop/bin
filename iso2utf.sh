#!/bin/bash

set -x

file_iso=$1
file_utf=${file_iso}.utf8

[ -f $file_iso ] || exit
iconv --from-code=ISO-8859-1 --to-code=UTF-8 $file_iso > $file_utf

