
TMPDIR=/tmp
MP3DIR=/dat/mp3/Hörspiel/wdr_hörspiele

cd $TMPDIR
wget  wget http://www.wdr.de/radio/home/downloadportal/download_hoerspiel.phtml
files=$(cat download_hoerspiel.phtml | grep "download/" | awk -F "\"" '{print $2}')
rm download_hoerspiel.phtml

[ -d $MP3DIR ] || mkdir -p $MP3DIR
cd $MP3DIR
wget -N $files
