#!/bin/bash

mountpoint=$1

[ -z $mountpoint ] && mountpoint=/media/hdext

#disks=$(fdisk -l | grep ^/dev/sd | gawk '{print $1}')
disks=$(cat /proc/partitions | egrep "sd[a-z][0-9]" | gawk '{printf("/dev/%s\n", $4)}')
mounted=$(mount | grep /dev/sd | gawk '{print $1}')

for d in $disks; do
    found=0
    for m in $mounted; do
	if [ $d == $m ]; then
	    found=1
	    break
	fi
    done
    if [ $found == 0 ]; then
        whoami
	echo "not found: $d -> mounting to $mountpoint"
        if [ "$mountpoint" != "" ]; then
	    sudo mount $d $mountpoint
            break
	fi
    fi
done

mounted=$(mount | grep -c $mountpoint )
if [ $mounted == 0 ]; then
    exit 1
fi
exit 0
