#!/bin/sh
REPOSITORY=pi@pi4.fritz.box:/media/hdext/borg
#REPOSITORY=pi@kodi.fritz.box:/media/hdext/borg
#REPOSITORY=pi@vdrpi.fritz.box:tmp/mnt/borg
PREFIX=nasdat

if [ "$1" != "--prune" ]; then

borg create -v --stats --progress --compression zlib --one-file-system       \
     $REPOSITORY::'nasdat-{now:%Y-%m-%d}'    \
     /dat/books \
     /dat/audio \
     /dat/docu/ \
     /dat/bak/db \
     --exclude '*/tmp' \
     --exclude '*.iso' \
     --exclude '/podcast/cache' \
     --exclude '/dat/docu/dropbox' \
     --exclude '/dat/docu/A' \
     --exclude '/dat/docu/E' \
     --exclude '/dat/docu/Download' \
     --exclude '*/files_trashbin' \

fi     

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machine's archives also.
borg prune -v --stats $REPOSITORY --prefix 'nasdat-' \
         --keep-daily=7 --keep-weekly=4 --keep-monthly=20
