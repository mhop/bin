#!/bin/bash

set -x

EXT_DRIVE=/media/hdext

REC_DIR=$1
VDR_DIR=`echo $REC_DIR | awk '{a=$0; gsub(/\/[^\/]*\.rec/,"",$a);print $a}'`
REC_NAME=`echo $REC_DIR | awk -F/ '{print $(NF-1)}'`
REC_NAME2=`echo $REC_DIR | awk -F/ '{print $(NF)}'`

function log {
    echo $1 >> /tmp/rollout.log 
}

function findRecInLinks {
    local MOUNTPOINT=$1
    local NAME=$2
    local DIR="$(find "${MOUNTPOINT}/links/" -maxdepth 2 -name "$NAME")"
    funcResult=$DIR
}

# find recording on dvd
function findRec {
    local MOUNTPOINT=$1
    local NAME=$2

    # try in directory 'links'
    findRecInLinks $MOUNTPOINT $NAME
    local DIR=$funcResult
    if [ -z "$DIR" ]; then
	# not found in links - try root
	DIR="$(find "${MOUNTPOINT}/" -maxdepth 2 -name "$NAME")"
  	# if not found, umount
 	if [ -z "$DIR" ]; then
	    log error "wrong dvd in drive / recording not found on dvd"
	    umount "$MOUNTPOINT" || { log error "dvd umount error (at: wrong dvd in drive / recording not found on dvd)"; exit 1; }
	    # If wanted, eject dvd
	    [ $EJECTWRONG -eq 1 ] && { eject "$DEVICE"; }
	    exit 3
	fi
    else
        local LINKTARGET=$(readlink $DIR)
        DIR=$(echo ${MOUNTPOINT}/links/$LINKTARGET)
    fi
    funcResult=$DIR
}

#mount $EXT_DRIVE
mountext.sh $EXT_DRIVE

if [ -f $EXT_DRIVE/vdrhd.num -a -f $REC_DIR/dvd.vdr ]; then
    cmp $REC_DIR/dvd.vdr $EXT_DRIVE/vdrhd.num
    if [ $? -eq 0 ]; then
        svdrpsend.pl MESG "Zur�ckholen gestarted"

        findRec $EXT_DRIVE $REC_NAME
        EXT_DIR=$funcResult
        log "Dir: $EXT_DIR"
        #EXT_DIR="$(find ${EXT_DRIVE} -name $REC_NAME)"
        #EXT_DIR="$(find ${EXT_DRIVE} -name $REC_NAME2)"
        if [ -d "$EXT_DIR" ]; then
            EXT_DIR_PAR=$(echo $EXT_DIR | awk '{a=$0; gsub(/\/[^\/]*\.rec/,"",$a);print $a}')
            log "rsync $EXT_DIR/ $REC_DIR/"
            log "recname $REC_NAME $REC_NAME2"
    	    echo rsync -av --include /[0.9]*\.vdr/ $EXT_DIR/ $REC_DIR/
    	    rsync -av --include /[0.9]*\.vdr/ $EXT_DIR/ $REC_DIR/
    	    if [ $? -eq 0 ] ; then
                echo rm $REC_DIR/dvd.vdr
                rm $REC_DIR/dvd.vdr
                echo rm -r $EXT_DIR_PAR
                rm -r $EXT_DIR_PAR
		findRecInLinks $EXT_DRIVE $REC_NAME
		LINK=$funcResult
		if [ ! -z $LINK ]; then
		    echo rm $LINK
		    rm $LINK
		fi
                #exit
                umount $EXT_DRIVE
                svdrpsend.pl MESG "Zur�ckholen abgeschlossen"
		exit
            else
                svdrpsend.pl MESG "Zur�ckholen fehlgeschlagen"
            fi
        else
            svdrpsend.pl MESG "externe Aufnahme nicht gefunden"
        fi
    else
        svdrpsend.pl MESG "Laufwerksnummer pa�t nicht"
    fi	
else
    svdrpsend.pl MESG "Kein DVD-Archive Laufwerk"
fi

umount $EXT_DRIVE



