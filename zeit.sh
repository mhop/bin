#!/bin/bash

destdir=/dat/books/zeit

user=mh256
pw=zeit0815
abonr=871

url="https://premium.zeit.de/premium/cgi-bin/_er_member/p4z.fpl?ER_Do=createToken&Abo_RedirectTo=epaper.zeit.de/index_premium.php&user=$user&aboxnr=$abonr"
url_zm="http://epaper.zeit.de/index_premium.php?user=$user&aboxnr=$abonr&magazin=ZM"
url_bl="http://epaper.zeit.de/index_premium.php?user=$user&aboxnr=$abonr&magazin=BL"
url_base=http://epaper.zeit.de

tmpfile=/tmp/zeit.tmp
tmpfile_zm=/tmp/zeit_zm.tmp
tmpfile_bl=/tmp/zeit_bl.tmp

[ -d $destdir ] || mkdir -p $destdir
cd $destdir

# links zum Download des pdf und epub ermitteln
wget --user=$user --password=$pw $url -O $tmpfile
pdf_url=$(cat $tmpfile    | grep Download | head -n 1 | sed -r 's/href=.(.*pdf).*/\1/')
epub_url=$(cat $tmpfile | grep content.*Download | awk -F "\"" '{print $2}')
# Nummer des Ausgabe zum umbenennen des epub ermitteln
number=$(echo $pdf_url | sed -r 's/^.*DZ_ePaper_(.*)\.pdf/\1/' | awk -F_ '{ printf("%s-%s",$2,$1)}' ) 
# links zum download von Beilage und Zeitmagazin ermitteln
wget --user=$user --password=$pw $url_zm -O $tmpfile_zm
pdfzm_url=$(cat $tmpfile_zm | grep Download | head -n 1 | sed -r 's/href=.(.*pdf).*/\1/')
wget --user=$user --password=$pw $url_bl -O $tmpfile_bl
pdfbl_url=$(cat $tmpfile_bl | grep Download | head -n 1 | sed -r 's/href=.(.*pdf).*/\1/')
# alles herunterladen
wget --user=$user --password=$pw $epub_url -O die_zeit_20$number.epub
wget --user=$user --password=$pw "$url_base/$pdf_url"
wget --user=$user --password=$pw "$url_base/$pdfzm_url"
wget --user=$user --password=$pw "$url_base/$pdfbl_url"
# Temp-files loeschen
rm $tmpfile $tmpfile_zm $tmpfile_bl



