require 'fileutils'

def split_all(path)
  head, tail = File.split(path)
  return [tail] if head == '.' || tail == '/'
  return [head, tail] if head == '/'
  return split_all(head) + [tail]
end


vidroot="/dat/video/vdr"
outdir="/dat/audio/vdr"
recdir=vidroot+"/"+ARGV.at(0)
#puts "p:#{p}"

# aus videopfad extraieren
dirsplitted=split_all(recdir)
if dirsplitted.length >= 2
  namedir=dirsplitted.at(-2).gsub("%", "")
  datedir=dirsplitted.at(-1)
end
#puts "dn:#{namedir}"

# Titel und Datum aus Infofile holen
infofile=recdir+"/info" 
infofile=recdir+"/info.vdr" unless File.exists?(infofile)
puts "infofile:#{infofile}"
title=open(infofile).grep(/^\s*T/).to_s.gsub("T","")
tim=open(infofile).grep(/^\s*E/).to_s.split(" ").at(2)
t=Time.at(tim.to_i)
#puts "time:#{t.to_s}"
#puts "tim:#{tim}"

#Zielverzeichnisbaum anlegen
outdir=outdir+"/"+namedir
Dir::mkdir(outdir) unless File.exists?(outdir)
outdir=outdir+"/"+datedir
Dir::mkdir(outdir) unless File.exists?(outdir)
# Infofile kopieren
FileUtils.cp(infofile, outdir)

# Kommando zum konvertieren 
recfile=recdir+"/00001.ts"
recfile=recdir+"/001.vdr" unless File.exists?(recfile)
title=title.to_s.strip
bitrate="128k" 
samplerate="44100"
outfile=outdir+"/"+namedir+".mp3"
author="vdr"
yr=t.year
cmd="avconv -i #{recfile} -acodec libmp3lame -f mp3 -metadata title='#{title}' -metadata author=#{author} -metadata year=#{yr} -ss 00:00:00 -ab #{bitrate} -ar #{samplerate} -threads auto #{outfile}"
puts "cmd:#{cmd}"
exec(cmd) 
puts "Fertig."
