export bakdir=/media/hdext

# Buecher -> jetzt ueber dirvish
#sudo rsync -vxrptgoL --progress --delete /dat/books/ $bakdir/books/

# Audio -> jetzt ueber dirvish
#sudo rsync -vxae --progress --delete /dat/mp3/ $bakdir/mp3/

# Dirvish: books, audio
sudo dirvish-expire
sudo dirvish-runall

# GPS
gpsdir=/dat/docu/gps/
sudo rsync -vxae --progress --delete --exclude 'garmin/' $gpsdir $bakdir/gps/

# vdr Struktur der Aufnahmen
videodir=/dat/video/vdr/
sudo -u vdr rsync -vxae --progress --delete --exclude '[0-9][0-9][0-9].vdr' --exclude '[0-9][0-9][0-9][0-9][0-9].ts' $videodir $bakdir/video/

srcdir=/dat/src
echo "#!/bin/bash" > /tmp/backuphelp
echo "dstdir=\$(dirname \$1)" >> /tmp/backuphelp
echo "mkdir -p \$bakdir/git\$dstdir" >> /tmp/backuphelp
echo "rsync -vxa --progress --delete \$1 \$bakdir/git\$dstdir" >> /tmp/backuphelp
find $srcdir -type d -name .git -exec bash -x  /tmp/backuphelp {} \;

