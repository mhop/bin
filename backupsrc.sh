#!/bin/sh
REPOSITORY=pi@pi4.fritz.box:/media/hdext/borg
#REPOSITORY=pi@kodi.fritz.box:/media/hdext/borg
#REPOSITORY=pi@vdrpi.fritz.box:tmp/mnt/borg
PREFIX=nassrc

if [ "$1" != "--prune" ]; then

borg create -v --stats --progress --compression zlib --one-file-system       \
     $REPOSITORY::'nassrc-{now:%Y-%m-%d}'    \
     /dat/src \
     --exclude '*/tmp/*' \
     --exclude '*/.build*/' \
     --exclude '*/build*/' \
     --exclude '*/rpi-build*/' \
     --exclude '*/dest*/' \
     --exclude '*/cache*' \
     --exclude '*/.cache/*' \
     --exclude '*/.ccache/*' \
     --exclude '*/subtree-cache/*' \
     --exclude '*/sstate*' \
     --exclude '*/dl/*' \
     --exclude '*/downloads/*' \
     --exclude '*/sourcemirror/*' \
     --exclude '*/yocto-dl-sources.git/*' \
     --exclude '*.o' \
     --exclude '*.ko' \
     --exclude '*.so' \
     --exclude '*.tar*' \
     --exclude '*.tgz' \
     --exclude '*.gz' \
     --exclude '*.zip' \
     --exclude '*/staging*/*' \
     --exclude '/run'

fi
     

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machine's archives also.
borg prune -v --stats $REPOSITORY --prefix 'nassrc-' \
         --keep-daily=7 --keep-weekly=4 --keep-monthly=20
