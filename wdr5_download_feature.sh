
TMPDIR=/tmp
MP3DIR=/dat/mp3/Hörspiel/wdr5_feature

cd $TMPDIR
wget http://www.wdr.de/radio/home/downloadportal/download_feature.phtml
file=$(cat download_feature.phtml | grep "\.mp3" | head -n 1 | awk -F "\"" '{print $2}')
rm download_feature.phtml

[ -d $MP3DIR ] || mkdir -p $MP3DIR
cd $MP3DIR
wget -N $file
