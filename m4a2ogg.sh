#!/bin/bash
#
# m4a to ogg

i=$1
   tmp=$(mktemp)
   y=`echo "$i"|sed -e 's/.m4a/.ogg/'`
   faad -i "$i" 1>/dev/null 2>"$tmp"
   if [ $? -ne 0 ] ; then
       rm "$tmp"
       echo "failed to get information from $i"
       continue
   fi
   title=`grep 'title: ' "$tmp"|sed -e 's/title: //'`
   artist=`grep 'artist: ' "$tmp"|sed -e 's/artist: //'`
   album=`grep 'album: ' "$tmp"|sed -e 's/album: //'`
   genre=`grep 'genre: ' "$tmp"|sed -e 's/genre: //'`
   track=`grep 'track: ' "$tmp"|sed -e 's/track: //'`
   year=`grep 'year: ' "$tmp"|sed -e 's/date: //'`
   faad "$i" -o - | oggenc -q 4 -t "$title" -a "$artist" -l "$album" -G "$genre" -N "$track" -d "$year" -o "$y" -
   if [ $? -ne 0 ] ; then
       echo "failed to encode $i"
   fi
   rm "$tmp"

