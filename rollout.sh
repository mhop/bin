#!/bin/bash

set -x

SRC=$1
DEST=/media/hdext
vdrarchivedir=/etc/vdr/archive

SVDRPORT=2001

svdrpsend -p $SVDRPORT MESG "Auslagern gestartet"

bash -x /usr/local/bin/mountext.sh $DEST
if [ $? != 0 ]; then
  svdrpsend -p $SVDRPORT MESG "Kann festplatte nicht mounten"
  exit
fi
	
sleep 5
SRC_DIR=$(echo $SRC | gawk '{a=$0; gsub(/\/[^\/]*\.rec/,"",$a);print $a}')
REC_DIR=$(echo $SRC | sed "s/\/$//g" | gawk -F '/' '{print $NF;}')
REC_DIR2=$(echo $SRC_DIR | sed "s/\/$//g" | gawk -F '/' '{print $NF;}')
echo RECD:: $REC_DIR2/$REC_DIR


mount


#ls -l $DEST
if [ -f $DEST/vdrhd.num ]; then
    if [ -d $SRC ]; then
	hdnum=$(cat $DEST/vdrhd.num | head -n 1)
        SRC_DIR2=$(echo $SRC_DIR | sed "s/\/$//g")
	echo nice -n 19 rsync -av "$SRC_DIR2" "$DEST/"
        #exit
	nice -n 19 rsync -av --progress "$SRC_DIR2" "$DEST/"
	if [ $? -eq 0 ] ; then
            #mkdir "$DEST/links"
            cd $DEST/links
            echo ln -s ../$REC_DIR2/$REC_DIR
            ln -s ../$REC_DIR2/$REC_DIR
            cd - 
            find "$SRC_DIR" -name *.rec -exec cp $DEST/vdrhd.num {}/dvd.vdr \;
            find $1 -name "[0-9]*.vdr" -exec rm {} \;
            find $1 -name "[0-9]*.ts" -exec rm {} \;
            free=$( df -h | grep $DEST | gawk '{print $4}' )
	    [ -d $vdrarchivedir ] || mkdir -p $vdrarchivedir 
	    echo "Hd:$hdnum Frei:$free ($(date))" > $vdrarchivedir/hd$hdnum.free
            sudo umount $DEST	
            sleep 15
            svdrpsend -p $SVDRPORT MESG "Auslagern abgeschlossen. $free frei"
            exit
	fi	
    else 
	svdrpsend -p $SVDRPORT MESG "Quellverzeichnis existert nicht"
    fi
else
    svdrpsend -p $SVDRPORT MESG "keine Auslagerfestplatte"
fi

free=$( df -h | grep $DEST | gawk '{print $4}' )
sudo umount $DEST
sleep 15	
svdrpsend.pl -p $SVDRPORT MESG "Auslagern fehlgeschlagen. $free frei"
