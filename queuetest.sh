

function QueueInit () {
    QueueCommandname=$(basename $1)
    QueueFile=/tmp/$QueueCommandname.queue
}

function QueueAdd() {
   echo $@ >> $QueueFile
}

#function QueueGet {
#}

function QueueAddAndExitWhenAlreadyRunning() {
    l=$(ps aux | grep $QueueCommandname | grep -v grep | wc -l)
    if [ $l -gt 2 ]; then
	echo "doppelt->Queue"
        QueueAdd $@
	exit
    fi
}

function QueueCheckAndGet() {
    if [ -f $QueueFile ]; then 
	l=$(cat $QueueFile | wc -l)
	if [ $l -ge 1 ]; then
	    new=$(cat $QueueFile | head -n 1 )
	    echo new:$new
            let l--
            cat $QueueFile | tail -n $l > $QueueFile.tmp
            cp $QueueFile.tmp $QueueFile
            QueueNextToRun=$new
            return 0
	fi
    fi
    return 1
}

function test() {
    echo "Test:"
    echo $1
    echo $@
}


read param
command=$(echo $param | awk '{print $1}')

QueueInit $command

QueueAddAndExitWhenAlreadyRunning $param

QueueCheckAndGet
if [ $? -eq 0 ]; then
    QueueAdd $param
    param=$QueueNextToRun
fi
run=1


while [ $run -eq 1 ]; do
    echo running $param
    $param
    echo finished $param
    date 

    QueueCheckAndGet
    if [ $? -eq 0 ]; then
	param=$QueueNextToRun
    else
        run=0;
    fi
done




